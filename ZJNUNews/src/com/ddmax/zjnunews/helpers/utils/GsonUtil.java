package com.ddmax.zjnunews.helpers.utils;

import android.text.TextUtils;

import com.ddmax.zjnunews.model.NewsDetailModel;
import com.ddmax.zjnunews.model.NewsListModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author ddMax
 * @since 2014-01-20
 * 说明：JSON解析工具类
 */
public class GsonUtil {

    public static NewsListModel getNewsList(String content) {

        if (TextUtils.isEmpty(content)) return null;

        Gson gson = new GsonBuilder().create();
        NewsListModel newsList = gson.fromJson(content, NewsListModel.class);

        return newsList != null ? newsList : null;
    }

    public static NewsDetailModel getNewsDetail(String content) {

        if (TextUtils.isEmpty(content)) return null;

        Gson gson = new GsonBuilder().create();
	    NewsDetailModel newsDetail = gson.fromJson(content, NewsDetailModel.class);

        return newsDetail != null ? newsDetail : null;
    }
}
