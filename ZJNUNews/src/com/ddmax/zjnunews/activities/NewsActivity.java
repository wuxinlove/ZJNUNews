package com.ddmax.zjnunews.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.ddmax.zjnunews.R;
import com.ddmax.zjnunews.helpers.adapters.BaseFragmentPagerAdapter;
import com.ddmax.zjnunews.ui.fragments.FindFragment;
import com.ddmax.zjnunews.ui.fragments.MoreFragment;
import com.ddmax.zjnunews.ui.fragments.NewsFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ddMax
 * @since 2015/02/19
 * 说明：新闻展示界面
 */
public class NewsActivity extends ActionBarActivity {

	// Fragments
	private List<Fragment> mFragments;
	private int mCurrentFragment = 0;
	// 中间内容
	private RelativeLayout mViewPagerContainer;
	private ViewPager mViewPager;
	private BaseFragmentPagerAdapter mFragmentPagerAdapter;
	// 顶部导航栏
	private RadioGroup mTopNaviBar;
	// 具体内容，用于fragment之间切换
	private android.support.v4.app.Fragment contentFragment;
	// 管理fragment
	private FragmentManager fragmentManager;
	// Toolbar
	private Toolbar mToolbar;

	public NewsActivity() {
	}

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news);

		// 初始化界面
		initUi();
		// 将Fragments放入List
		initFragments();
		// 初始化ViewPager滑动
		initViewPager();
	}

	// 实现再按一次返回键退出的功能
	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	protected void onPause() {
		super.onPause();

	}

	private void initFragments() {
		mFragments = new ArrayList<>();
		mFragments.add(new NewsFragment());
		mFragments.add(new FindFragment());
		mFragments.add(new MoreFragment());
	}

	private void initViewPager() {
		// 中间内容
		mViewPager = (ViewPager) findViewById(R.id.view_pager);
		mViewPagerContainer = (RelativeLayout) findViewById(R.id.news_container);
		// ViewPager属性设置
		mFragmentPagerAdapter = new BaseFragmentPagerAdapter(this.getSupportFragmentManager(), mViewPager, mFragments);
		mViewPager.setOnPageChangeListener(this.mPageChangeListener);
		mViewPagerContainer.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return mViewPager.dispatchTouchEvent(event);
			}
		});
	}

	/**
	 * 初始化界面
	 */
	private void initUi() {

		// 顶部导航栏
		mTopNaviBar = (RadioGroup) findViewById(R.id.topNaviBar);
		mTopNaviBar.setOnCheckedChangeListener(itemChanged);
		// fragment管理器
		fragmentManager = getSupportFragmentManager();
		// 设置初始页面为校园新闻
		contentFragment = new NewsFragment();
		fragmentManager.beginTransaction().replace(R.id.view_pager, contentFragment).commit();

		// 初始化Toolbar，设置Toolbar菜单，返回
		mToolbar = (Toolbar) findViewById(R.id.mToolbar);
		setSupportActionBar(mToolbar);
		mToolbar.inflateMenu(R.menu.menu_news);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		mToolbar.setOnMenuItemClickListener(mOnMenuItemClickListener);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// 填充Toolbar上的菜单项
		getMenuInflater().inflate(R.menu.menu_news, menu);
		return true;
	}

	/**
	 * 顶部导航栏监听事件
	 */
	private RadioGroup.OnCheckedChangeListener itemChanged = new RadioGroup.OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			int checkedItem = 0;
			switch (checkedId) {
				case R.id.rbNews:
					checkedItem = 0;
					break;
				case R.id.rbFind:
					checkedItem = 1;
					break;
				case R.id.rbMore:
					checkedItem = 2;
					break;
				default:
					break;
			}
			mViewPager.setCurrentItem(checkedItem);
			mCurrentFragment = checkedItem;
		}
	};

	/**
	 * ViewPager滑动监听事件
	 */
	private ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {
		@Override
		public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
			if (mViewPager != null) {
				mViewPager.invalidate();
			}
		}

		@Override
		public void onPageSelected(int position) {
			mCurrentFragment = position;
			((RadioButton) mTopNaviBar.getChildAt(position)).setChecked(true);
		}

		@Override
		public void onPageScrollStateChanged(int state) {

		}
	};

	/**
	 * Toolbar菜单点击监听事件
	 */
	public Toolbar.OnMenuItemClickListener mOnMenuItemClickListener = new Toolbar.OnMenuItemClickListener() {
		@Override
		public boolean onMenuItemClick(MenuItem item) {
			int id = item.getItemId();
			switch (id) {
				case R.id.home:
					finish();
				default:
					break;
			}
			return false;
		}
	};

}
