package com.ddmax.zjnunews.task;

import android.content.Context;

import com.ddmax.zjnunews.Constants;
import com.ddmax.zjnunews.helpers.utils.GsonUtil;
import com.ddmax.zjnunews.model.NewsDetailModel;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * @author ddMax
 * @since 2015/02/13
 * 说明：后台获取新闻详情
 */
public class GetNewsDetailTask extends BaseGetNewsTask<NewsDetailModel> {

	public GetNewsDetailTask(Context mContext, ResponseListener mResponseListener) {
		super(mContext, mResponseListener);
	}

	@Override
	protected NewsDetailModel doInBackground(String... params) {

		if (params.length == 0) {
			return null;
		}

		String detailContent;
		NewsDetailModel mNewsDetailModel = null;

		try {
			detailContent = getUrl(Constants.URL.NEWSDETAIL + params[0]);
			mNewsDetailModel = GsonUtil.getNewsDetail(detailContent);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return mNewsDetailModel;
	}
}
