package com.ddmax.zjnunews.task;

import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;

/**
 * @author ddMax
 * @since 2015/01/26 13:24.
 */
public abstract class BaseGetNewsTask<T> extends AsyncTask<String, String, T> {

	protected Context mContext = null;
	protected ResponseListener mResponseListener = null;
	protected Exception e = null;

	public BaseGetNewsTask(Context mContext, ResponseListener mResponseListener) {
		super();
		this.mContext = mContext;
		this.mResponseListener = mResponseListener;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		if (mResponseListener != null) {
			mResponseListener.onPreExecute();
		}
	}

	@Override
	protected void onPostExecute(T response) {
		super.onPostExecute(response);

		if (mResponseListener != null) {
			mResponseListener.onPostExecute(response);
		}
	}

	@Override
	protected void onProgressUpdate(String... values) {
		super.onProgressUpdate(values);
	}

	protected String getUrl(String url) throws IOException {

		HttpGet request = new HttpGet(url);
		HttpClient httpClient = new DefaultHttpClient();

		httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY,
				CookiePolicy.BROWSER_COMPATIBILITY);

		return httpClient.execute(request, new BasicResponseHandler());

	}

//	public static interface ResponseListener {
//
//		public void onPreExecute();
//
//		// TODO: More args
//		public void onPostExecute(ArrayList<NewsModel> result);
//
//		public void onProgressUpdate(String value);
//
//		public void onFail(Exception e);
//	}

}
