package com.ddmax.zjnunews.task;

import com.ddmax.zjnunews.model.NewsModel;

import java.util.ArrayList;

/**
 * @author ddMax
 * @since 2015/02/14 15:24.
 * 说明：Task回调接口
 */
public interface ResponseListener<T> {

	public void onPreExecute();

	// TODO: More args
	public void onPostExecute(T result);

	public void onProgressUpdate(String value);

	public void onFail(Exception e);

}
