package com.ddmax.zjnunews;

public final class Constants {

	// 各模块代号定义

	public static final class MODULE {

		public static final int NEWS = 0;
		public static final int CALENDAR = 1;
		public static final int BUS = 2;
		public static final int SPEECH = 3;
		public static final int RESOURCES = 4;
	}

	// 数信学工办新闻列表地址
    public static final class URL {

		// 学工新闻
		public static final String NEWS_LEARNING = "http://slxx.zjnu.edu.cn/xgb/listIndexInfo.action?index=1";

		// 信息通告
		public static final String NEWS_NOTIFICATION = "http://slxx.zjnu.edu.cn/xgb/listIndexInfo.action?index=2";

        // 新闻内容地址
        public static final String NEWSDETAIL = "http://slxx.zjnu.edu.cn/xgb/showInfo.action?id=";
    }
	public static final String TEMPLATE_NEWS_DETAIL = "news-templates/template.html";


}
