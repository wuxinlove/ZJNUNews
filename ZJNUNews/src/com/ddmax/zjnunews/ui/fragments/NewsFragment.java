package com.ddmax.zjnunews.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ddmax.zjnunews.Constants;
import com.ddmax.zjnunews.R;
import com.ddmax.zjnunews.activities.NewsDetailActivity;
import com.ddmax.zjnunews.helpers.adapters.NewsListAdapter;
import com.ddmax.zjnunews.model.NewsModel;
import com.ddmax.zjnunews.task.ResponseListener;
import com.ddmax.zjnunews.task.GetNewsTask;

import java.util.ArrayList;

/**
 * @author ddMax
 * @since 2014/12/31
 * 说明：新闻列表Fragment
 */
public class NewsFragment extends Fragment implements AdapterView.OnItemClickListener, ResponseListener<ArrayList<NewsModel>> {
    private final static int REFRESH_STATUS = 0;

    private ListView mList;
    private SwipeRefreshLayout mLayout;

    // 新闻列表
    private ArrayList<NewsModel> mNewsList;
    private NewsListAdapter mAdapter;

    private Handler refreshHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case REFRESH_STATUS:
                    if (mNewsList != null) {
                        loadNewsList();
                        mAdapter.notifyDataSetChanged();
                    }
                    mLayout.setRefreshing(false);
                    break;

                default:
                    break;
            }
        }

    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

	    // TODO: 新闻列表缓存

	    //加载新闻
	    loadNewsList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
		// 填充Fragment布局
	    ViewGroup mViewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_news, container, false);
	    // 设置新闻
	    mList = (ListView) mViewGroup.findViewById(R.id.newsList);
        // 设置下拉监听事件
        mLayout = (SwipeRefreshLayout) mViewGroup.findViewById(R.id.swipeRefreshLayout);
        mLayout.setOnRefreshListener(new OnRefreshListener() {

            @Override
            public void onRefresh() {
                refreshHandler.sendEmptyMessageDelayed(REFRESH_STATUS, 222);
            }
        });
        // 设置下拉主题
        mLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.holo_green_light, R.color.holo_orange_light, R.color.holo_red_light);

        return mViewGroup;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

	// 加载新闻列表
    public void loadNewsList(){

	    new GetNewsTask(getActivity(), this).execute(Constants.URL.NEWS_LEARNING);
    }

	// 以下为ResponseListener接口方法实现
	@Override
	public void onPreExecute() {

	}

	@Override
	public void onPostExecute(ArrayList<NewsModel> result) {
		if (isAdded()) {
			mNewsList = result;
			mAdapter = new NewsListAdapter(getActivity(), result);
			mList.setAdapter(mAdapter);
			// 设置新闻列表点击监听事件
			mList.setOnItemClickListener(this);
		}
	}

	@Override
	public void onProgressUpdate(String value) {

	}

	@Override
	public void onFail(Exception e) {

	}

	// ListView点击监听事件
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

		NewsModel mNewsModel = mNewsList != null ? mNewsList.get(position) : null;

		if (mNewsModel == null) {
			return;
		}

		Intent intent = new Intent();
		intent.putExtra("id", mNewsModel.getId());
		intent.putExtra("newsModel", mNewsModel);

		// 跳转到NewsDetailActivity
		intent.setClass(getActivity(), NewsDetailActivity.class);
		getActivity().startActivity(intent);
	}
}
